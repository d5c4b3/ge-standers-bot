create table snakes_and_ladders
(
    guild_id text primary key not null,
    watch_channel text not null,
    message_channel text not null,
    data     blob not null
);

create index snakes_and_ladders_ix_guild_id on snakes_and_ladders (guild_id);
