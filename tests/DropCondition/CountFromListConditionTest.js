import {describe, it, beforeEach} from 'node:test';
import assert from 'node:assert/strict';
import {CountFromListCondition} from "../../src/DropCondition/CountFromListCondition.js";

describe('CountFromList', () => {
  let condition;

  beforeEach(() => {
    condition = new CountFromListCondition([
      {drop: 'Vorkath head', count: 3},
      {drop: 'Dragon longsword', count: 1},
    ]);
  });

  it('should not fulfill without correct drops', () => {
    assert.equal(condition.isFulfilled([]), false);
    assert.equal(condition.isFulfilled(['Vorkath head', 'Vorkath head']), false);
    assert.equal(condition.isFulfilled(['Another random drop']), false);
  });

  it('should be case insensitive', () => {
    assert.equal(condition.isFulfilled(['dragon LONGSWORD']), true);
    assert.equal(condition.isFulfilled(['Vorkath head', 'VORKATH HEAD', 'vorkath head']), true);
  });

  it('should be relevant for relevant drops', () => {
    assert.equal(condition.isRelevant('Dragon longsword'), true);
    assert.equal(condition.isRelevant('Vorkath head'), true);
    assert.equal(condition.isRelevant('Another random drop'), false);
  });
});
