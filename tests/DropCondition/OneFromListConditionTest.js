import {describe, it, beforeEach} from 'node:test';
import assert from 'node:assert/strict';
import {OneFromListCondition} from "../../src/DropCondition/OneFromListCondition.js";

describe('OneFromList', () => {
  let condition;

  beforeEach(() => {
    condition = new OneFromListCondition([
      'Dragon med helm',
      'Dragon 2h sword',
      'Bones'
    ]);
  });

  it('should not fulfill without correct drops', () => {
    assert.equal(condition.isFulfilled([]), false);
    assert.equal(condition.isFulfilled(['Dragon boots']), false);
    assert.equal(condition.isFulfilled(['Another random drop']), false);
  });

  it('should be case insensitive', () => {
    assert.equal(condition.isFulfilled(['BONES']), true);
    assert.equal(condition.isFulfilled(['dragon med helm']), true);
  });

  it('should be relevant for relevant drops', () => {
    assert.equal(condition.isRelevant('Bones'), true);
    assert.equal(condition.isRelevant('Dragon 2h sword'), true);
    assert.equal(condition.isRelevant('Another random drop'), false);
  });
});
