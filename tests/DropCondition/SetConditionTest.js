import {describe, it, beforeEach} from 'node:test';
import assert from 'node:assert/strict';
import {SetCondition} from "../../src/DropCondition/SetCondition.js";

describe('SetCondition', () => {
  let condition_no_dupes;
  let condition_with_dupes;

  beforeEach(() => {
    condition_no_dupes = new SetCondition([
      [
        'Arcane prayer scroll',
        'Dexterous prayer scroll',
      ],
      [
        'Bryophyta essence',
        'Hill giant club',
      ]
    ]);

    condition_with_dupes = new SetCondition([
      ['Masori mask','Masori mask'],
      ['Masori body','Masori body'],
      ['Masori chaps','Masori chaps'],
      ['Masori mask','Masori body'],
      ['Masori mask','Masori chaps'],
      ['Masori body','Masori chaps'],
    ]);
  });

  it('should not fulfill without correct drops', () => {
    assert.equal(condition_no_dupes.isFulfilled([]), false);
    assert.equal(condition_no_dupes.isFulfilled(['Dragon boots', 'Adamant longsword']), false);
    assert.equal(condition_no_dupes.isFulfilled(['Another random drop']), false);
  });

  it('should be case insensitive', () => {
    assert.equal(condition_no_dupes.isFulfilled(['Arcane PRAYER scroll', 'dexterous prayer scroll']), true);
  });

  it('should work on either set', () => {
    assert.equal(condition_no_dupes.isFulfilled(['Arcane prayer scroll', 'Bryophyta essence', 'Hill giant club']), true);
  });

  it('should work if you have dupes', () => {
    assert.equal(condition_with_dupes.isFulfilled(['Masori mask', 'Masori mask']), true);
    assert.equal(condition_with_dupes.isFulfilled(['Masori mask', 'Masori body']), true);
  });

  it('should be relevant for relevant drops', () => {
    assert.equal(condition_no_dupes.isRelevant('Hill giant club'), true);
    assert.equal(condition_with_dupes.isRelevant('Masori chaps'), true);
    assert.equal(condition_no_dupes.isRelevant('Another random drop'), false);
  });
});
