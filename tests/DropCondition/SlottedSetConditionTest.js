import {describe, it, beforeEach} from 'node:test';
import assert from 'node:assert/strict';
import {SlottedSetCondition} from "../../src/DropCondition/SlottedSetCondition.js";

describe('SlottedSetCondition', () => {
  let condition;

  beforeEach(() => {
    condition = new SlottedSetCondition([
      {
        name: 'Helm',
        drops: [
          'Ahrim hood',
          'Dharok helm',
          'Guthan helm',
        ]
      },
      {
        name: 'Body',
        drops: [
          'Ahrim robetop',
          'Dharok platebody',
          'Guthan platebody',
        ]
      }
    ]);
  });

  it('should not fulfill without correct drops', () => {
    assert.equal(condition.isFulfilled([]), false);
    assert.equal(condition.isFulfilled(['Dragon boots', 'Adamant longsword']), false);
    assert.equal(condition.isFulfilled(['Another random drop']), false);
  });

  it('should be case insensitive', () => {
    assert.equal(condition.isFulfilled(['ahrim robetop', 'Dharok HELM']), true);
  });

  it('should be relevant for relevant drops', () => {
    assert.equal(condition.isRelevant('Guthan platebody'), true);
    assert.equal(condition.isRelevant('Ahrim hood'), true);
    assert.equal(condition.isRelevant('Another random drop'), false);
  });
});
