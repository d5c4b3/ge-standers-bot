import {describe, it, beforeEach} from 'node:test';
import assert from 'node:assert/strict';
import {AlwaysFalseCondition} from "../../src/DropCondition/AlwaysFalseCondition.js";

describe('AlwaysFalseCondition', () => {
  let condition;

  beforeEach(() => {
    condition = new AlwaysFalseCondition();
  })

  it('should always be false', () => {
    assert.equal(condition.isFulfilled(['drops']), false);
  });

  it('should never be relevant', () => {
    assert.equal(condition.isRelevant('drop'), false);
  });
});
