import {describe, it, beforeEach} from 'node:test';
import assert from 'node:assert/strict';
import {Board} from "../../src/SnakesAndLadders/Board.js";
import {Tile} from "../../src/SnakesAndLadders/Tile.js";
import {AlwaysFalseCondition} from "../../src/DropCondition/AlwaysFalseCondition.js";


describe('moveTile', () => {
  const tiles = [
    new Tile(0, 'tile 0', '', new AlwaysFalseCondition()),
    new Tile(1, 'tile 1', '', new AlwaysFalseCondition()),
    new Tile(2, 'tile 2', '', new AlwaysFalseCondition()),
    new Tile(3, 'tile 3', '', new AlwaysFalseCondition()),
    new Tile(4, 'tile 4', '', null, 2),
    new Tile(5, 'tile 5', '', new AlwaysFalseCondition()),
    new Tile(6, 'tile 7', '', new AlwaysFalseCondition()),
    new Tile(7, 'tile 8', '', new AlwaysFalseCondition()),
    new Tile(8, 'tile 9', '', new AlwaysFalseCondition()),
    new Tile(9, 'tile 10', '', new AlwaysFalseCondition()),
    new Tile(10, 'tile 6', '', null, 8),
  ];
  const board = new Board(tiles);
  board.moveTile(tiles[10], 6);

  const tile6 = board.tiles[6];

  it('should have put the 10th tile to the 6th spot', () => {
    assert.equal(tile6.name, 'tile 6');
  });

  it('should still have the proper goto', () => {
    assert.equal(tile6.goto, 9);
    assert.equal(board.tiles[4].goto, 2);
  });

  it('should have pushed tile10 to the 10th spot', () => {
    assert.equal(board.getTile(10).name, 'tile 10');
  });
});

describe('insertTile', () => {
  let board;

  beforeEach(() => {
    const tiles = [
      new Tile(0, 'tile 0', '', new AlwaysFalseCondition()),
      new Tile(1, 'tile 1', '', null, 0),
      new Tile(2, 'tile 2', '', null, 3),
      new Tile(3, 'tile 3', '', new AlwaysFalseCondition()),
    ];
    board = new Board(tiles);
  });

  it('should have another tile after insert', () => {
    board.insertTile(4, new Tile(4, 'tile 4', '', new AlwaysFalseCondition()))
    assert.equal(board.tiles.length, 5);
  });

  it('should be at index 4', () => {
    const tile = new Tile(4, 'tile 4', '', new AlwaysFalseCondition());
    board.insertTile(4, tile);
    assert.equal(board.tiles[4].name, 'tile 4');
  });

  it('should be at index 0', () => {
    const tile = new Tile(4, 'tile 4', '', new AlwaysFalseCondition());
    board.insertTile(0, tile);
    assert.equal(board.tiles[0].name, 'tile 4');
    assert.equal(board.tiles[0].index, 0);
  });

  it('should be at index 2', () => {
    const tile = new Tile(4, 'tile 4', '', new AlwaysFalseCondition());
    board.insertTile(2, tile);
    assert.equal(board.tiles[2].name, 'tile 4');
    assert.equal(board.tiles[2].index, 2);
  });

  it('should have maintained gotos (end)', () => {
    const tile = new Tile(4, 'tile 4', '', new AlwaysFalseCondition());
    board.insertTile(4, tile);
    assert.equal(board.tiles[1].goto, 0);
    assert.equal(board.tiles[2].goto, 3);
  });

  it('should have maintained gotos (begin)', () => {
    const tile = new Tile(4, 'tile 4', '', new AlwaysFalseCondition());
    board.insertTile(0, tile);
    assert.equal(board.tiles[2].goto, 1);
    assert.equal(board.tiles[3].goto, 4);
  });

  it('shouldn\'t update gotos past end of board', () => {
    board.tiles[2].goto = 5;
    const tile = new Tile(4, 'tile 4', '', new AlwaysFalseCondition());
    board.insertTile(4, tile);
    assert.equal(board.tiles[2].goto, 5);
  });
});
