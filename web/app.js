import {TileSerializer} from "../src/Serializer/SnakesAndLadders/TileSerializer.js";
import {Tile} from "../src/SnakesAndLadders/Tile.js";
import {ConditionListSerializer} from "../src/Serializer/ConditionListSerializer.js";

const serializer = new TileSerializer();
const condition_serializer = new ConditionListSerializer();

document.getElementById('tile-form').addEventListener('submit', function(e) {
  e.preventDefault();

  const name = document.getElementById('tile-name').value;
  const desc = document.getElementById('tile-description').value;
  const isSnakeLadder = document.getElementById('is-snake-ladder').checked;

  let tile;
  if (isSnakeLadder) {
    let goto = parseInt(document.getElementById('tile-goto').value.trim());
    goto--; // Account for 1-indexed display, but 0-indexed data
    tile = new Tile(0, name, desc, null, goto);
  } else {
    const con_type = document.getElementById('tile-condition-type').value;
    const con_data = document.getElementById('tile-condition').value;
    const condition = condition_serializer.parse(con_data, con_type);
    tile = new Tile(0, name, desc, condition);
  }

  const json = serializer.serialize(tile, true);
  document.getElementById('tile-json').innerText = json;
  document.getElementById('output-wrapper').style.display = '';
});
