FROM node:21

WORKDIR /app

RUN apt-get -y update && apt-get install -y sqlite3

COPY package.json package-lock.json /app/
RUN npm install

COPY . /app

RUN chmod +x docker/entrypoint.sh

ENTRYPOINT ["docker/entrypoint.sh"]
CMD ["npm", "start"]
