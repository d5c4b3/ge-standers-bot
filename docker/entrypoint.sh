#!/bin/sh

set -e

# Set up the db schema
if [ ! -f ./var/data.db ]; then
  mkdir -p ./var
  sqlite3 ./var/data.db < ./migrations/001-initial.sql
fi

exec "$@"
