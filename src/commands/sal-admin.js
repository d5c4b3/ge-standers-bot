import {Subcommand} from "@sapphire/plugin-subcommands";
import {EventAdminHandler} from "../Discord/Sal/EventAdminHandler.js";
import {Util} from "../Discord/Sal/Util.js";
import {MessageCreateDropsListener} from "../listeners/MessageCreateDropsListener.js";
import {TeamAdminHandler} from "../Discord/Sal/TeamAdminHandler.js";
import {TileAdminHandler} from "../Discord/Sal/TileAdminHandler.js";

class SalAdminCommand extends Subcommand {
  constructor(context, options) {
    const subcommands = [
      {
        name: 'roll',
        chatInputRun: 'chatInputRoll'
      },
      {
        name: 'reroll',
        chatInputRun: 'chatInputReroll'
      },
      {
        name: 'force-check-drop-message',
        chatInputRun: 'chatInputForceCheckDropMessage'
      },
    ];

    const handlers = [
      new EventAdminHandler(),
      new TeamAdminHandler(),
      new TileAdminHandler(),
    ];

    for (const handler of handlers) {
      subcommands.push(...handler.getSubcommandMappings());
    }

    super(context, {
      ...options,
      name: 'sal-admin',
      subcommands: subcommands
    });

    this.handlers = handlers;
  }

  async chatInputRoll(interaction, context) {
    const event = await Util.getEvent(interaction);

    const team = event.snakes_and_ladders.getTeam(interaction.options.getString('team', true));
    if (!team) {
      return interaction.reply({
        content: `Could not find a team with that name`
      });
    }

    const roll = event.snakes_and_ladders.rollTurn(team);

    await Util.persistEvent(interaction, event);

    await Util.sendRollResultMessage(interaction, event, team, roll, false);

    await interaction.deferReply();
    await interaction.deleteReply();
  }

  async chatInputReroll(interaction, context) {
    const event = await Util.getEvent(interaction);

    const team = event.snakes_and_ladders.getTeam(interaction.options.getString('team', true));
    if (!team) {
      return interaction.reply({
        content: `Could not find a team with that name`
      });
    }

    const roll = event.snakes_and_ladders.rerollTurn(team);

    await Util.persistEvent(interaction, event);

    await Util.sendRollResultMessage(interaction, event, team, roll, false);

    await interaction.deferReply();
    await interaction.deleteReply();
  }

  /**
   * @param {ChatInputCommandInteraction} interaction
   * @param context
   * @return {Promise<void>}
   */
  async chatInputForceCheckDropMessage(interaction, context) {
    const event = await Util.getEvent(interaction);
    const message_id = interaction.options.getString('message-id', true);
    const message = await interaction.client.channels.cache.get(event.watch_channel).messages.fetch(message_id);

    await MessageCreateDropsListener.handleDropMessage(message, true);

    await interaction.deferReply();
    await interaction.deleteReply();
  }

  registerApplicationCommands(registry) {
    registry.registerChatInputCommand((builder) => {
      builder
        .setName(this.name)
        .setDescription('A collection of Snakes and Ladders commands');

      for (const handler of this.handlers) {
        handler.registerSubcommands(builder);
      }

      builder
        .addSubcommand((builder) =>
          builder
            .setName('roll')
            .setDescription('Manually rolls a turn for a team')
            .addStringOption((option) =>
              option
                .setName('team')
                .setDescription('The team to roll for')
                .setRequired(true)
                .setAutocomplete(true)
            )
        )
        .addSubcommand((builder) =>
          builder
            .setName('reroll')
            .setDescription('Re-rolls a turn for a team')
            .addStringOption((option) =>
              option
                .setName('team')
                .setDescription('The team to roll for')
                .setRequired(true)
                .setAutocomplete(true)
            )
        )
        // TODO: only register this if the debug variable is set
        .addSubcommand((builder) =>
          builder
            .setName('force-check-drop-message')
            .setDescription('Triggers the drop listener for a message. The message MUST be in the configured watch channel.')
            .addStringOption(option =>
              option
                .setName('message-id')
                .setDescription('The id of the drop message to debug')
                .setRequired(true)
            )
        )

      return builder;
    });
  }
}

export {SalAdminCommand}
