import {Subcommand} from "@sapphire/plugin-subcommands";
import {InfoHandler} from "../Discord/Sal/InfoHandler.js";
import {Util} from "../Discord/Sal/Util.js";
import {SnakesAndLaddersRenderer} from "../Renderer/SnakesAndLaddersRenderer.js";
import {container} from "@sapphire/framework";
import fs from "fs";

class SalCommand extends Subcommand {
  constructor(context, options) {
    const subcommands = [
      {
        name: 'show-board',
        chatInputRun: 'chatInputShowBoard'
      },
    ];

    const handlers = [
      new InfoHandler(),
    ];

    for (const handler of handlers) {
      subcommands.push(...handler.getSubcommandMappings());
    }

    super(context, {
      ...options,
      name: 'sal',
      subcommands: subcommands
    });

    this.handlers = handlers;

  }

  registerApplicationCommands(registry) {
    registry.registerChatInputCommand((builder) => {
      builder
        .setName(this.name)
        .setDescription('A collection of Snakes and Ladders commands')
        .addSubcommand((builder) =>
          builder
            .setName('show-board')
            .setDescription('Displays a (poorly) rendered version of the current board')
        );

      for (const handler of this.handlers) {
        handler.registerSubcommands(builder);
      }

      return builder;
    });
  }

  async chatInputShowBoard(interaction, context) {
    const event = await Util.getEvent(interaction);
    const renderer = new SnakesAndLaddersRenderer();

    const canvas = renderer.render(event.snakes_and_ladders);
    const buffer = canvas.toBuffer('image/png');
    const file_path = `${container.image_cache_path}/${event.guild_id}.png`;
    fs.writeFileSync(file_path, buffer);

    interaction.reply({
      files: [file_path],
    });
  }

}

export {SalCommand};
