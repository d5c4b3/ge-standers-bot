import {Util} from "./Util.js";

class CommonMessages {}

CommonMessages.buildRollMessage = function(event, team, roll) {
  const tile_display = Util.toDisplayIndex(roll.tile_index);
  let landing_on_msg = `landing on tile ${tile_display}`;
  if (roll.ladder_from_index !== null) {
    const ladder_tile = Util.toDisplayIndex(roll.ladder_from_index);
    landing_on_msg = `landing on a ladder (${ladder_tile}), stepping them up to ${tile_display}`;
  } else if (roll.snake_from_index !== null) {
    const snake_tile = Util.toDisplayIndex(roll.snake_from_index);
    landing_on_msg = `landing on a chute (${snake_tile}), shooting them down to ${tile_display}`;
  }

  let roll_display = `You rolled a ${roll.sum}`;
  if (roll.rolls.length > 1) {
    roll_display +=  ` (${roll.rolls.join(', ')})`
  }

  return `${roll_display} ${landing_on_msg}`;
}

export {CommonMessages}
