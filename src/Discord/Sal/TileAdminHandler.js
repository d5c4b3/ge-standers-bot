import {Util} from "./Util.js";
import {DisplayableError} from "../Error/DisplayableError.js";
import {TileSerializer} from "../../Serializer/SnakesAndLadders/TileSerializer.js";

class TileAdminHandler {

  constructor() {
    this.tileSerializer = new TileSerializer();
  }

  //TODO: replace tile?

  async chatInputCreate(interaction) {
    //TODO: add tile
  }

  async chatInputDelete(interaction) {
    const event = await Util.getEvent(interaction);
    let index = interaction.options.getInteger('tile-number', true);
    index = Util.fromInputIndex(index);

    const tile = event.snakes_and_ladders.board.getTile(index);

    if (!tile) {
      interaction.reply({
        content: 'No tile to delete',
      });
      return;
    }

    event.snakes_and_ladders.deleteTile(index);
    await Util.persistEvent(interaction, event);

    interaction.reply({
      content: `Tile successfully deleted: ${tile.name}`
    });
  }

  async chatInputEdit(interaction) {
    //TODO: update tile
  }

  async chatInputMove(interaction) {
    const event = await Util.getEvent(interaction);
    const tile_index = interaction.options.getInteger('from-tile-number', true);
    const tile = event.snakes_and_ladders.board.getTile(Util.fromInputIndex(tile_index));

    if (!tile)
      throw new DisplayableError(`Can't find tile ${tile_index}`);

    const to_index = Util.fromInputIndex(interaction.options.getInteger('to-tile-number', true));
    event.snakes_and_ladders.board.moveTile(tile, to_index);
    await Util.persistEvent(interaction, event);

    interaction.reply({
      content: 'Tile moved'
    });
  }

  async chatInputImport(interaction) {
    const event = await Util.getEvent(interaction);
    const tile = this.tileSerializer.parse(interaction.options.getString('tile-data', true));
    let index = interaction.options.getInteger('tile-number', false);

    if (index === null) {
      index = event.snakes_and_ladders.board.tiles.length;
    } else {
      index = Util.fromInputIndex(index);
    }

    event.snakes_and_ladders.insertTile(index, tile);
    await Util.persistEvent(interaction, event);

    interaction.reply({
      content: 'Tile imported'
    });
  }

  async chatInputExport(interaction) {
    const event = await Util.getEvent(interaction);
    const index = interaction.options.getInteger('tile-number', true);
    const tile = event.snakes_and_ladders.board.getTile(Util.fromInputIndex(index));

    if (!tile)
      throw new DisplayableError(`Can't find tile ${index}`);

    const data = this.tileSerializer.serialize(tile);

    interaction.reply({
      content: '```json\n' + data + '\n```'
    })
  }

  async chatInputSetGoto(interaction) {
    const event = await Util.getEvent(interaction);
    const index = interaction.options.getInteger('tile-number', true);
    const tile = TileAdminHandler.getTile(interaction, event, index);
    let goto = interaction.options.getInteger('goto', true);
    goto = Util.fromInputIndex(goto);

    if (tile.goto === null) {
      throw new DisplayableError(`Can't set goto on tile with condition`);
    }

    tile.goto = goto;
    await Util.persistEvent(interaction, event);

    interaction.reply({
      content: `Tile (${Util.toDisplayIndex(tile.index)}) updated`
    });
  }

  static getTile(interaction, event, index) {
    const tile = event.snakes_and_ladders.board.getTile(Util.fromInputIndex(index));

    if (!tile)
      throw new DisplayableError(`Can't find tile ${index}`);

    return tile;
  }

  getSubcommandMappings() {
    return [
      {
        name: 'tile',
        type: 'group',
        entries: [
          // {
          //   name: 'create',
          //   chatInputRun: this.chatInputCreate.bind(this),
          // },
          {
            name: 'delete',
            chatInputRun: this.chatInputDelete.bind(this),
          },
          {
            name: 'move',
            chatInputRun: this.chatInputMove.bind(this),
          },
          {
            name: 'import',
            chatInputRun: this.chatInputImport.bind(this),
          },
          {
            name: 'export',
            chatInputRun: this.chatInputExport.bind(this),
          },
          {
            name: 'set-goto',
            chatInputRun: this.chatInputSetGoto.bind(this),
          }
        ]
      },
    ];
  }

  registerSubcommands(builder) {
    builder
      .addSubcommandGroup(builder =>
        builder
          .setName('tile').setDescription('Tile commands')
          // .addSubcommand(builder =>
          //   builder
          //     .setName('create')
          //     .setDescription('Create a new tile')
          // )
          // .addSubcommand((builder) =>
          //   builder
          //     .setName('list')
          //     .setDescription('Displays all the tiles')
          // )
          .addSubcommand((builder) =>
            builder
              .setName('move')
              .setDescription('Moves a tile from one spot to another')
              .addIntegerOption((option) =>
                option
                  .setName('from-tile-number')
                  .setDescription('The number of the tile to move')
                  .setRequired(true)
              )
              .addIntegerOption((option) =>
                option
                  .setName('to-tile-number')
                  .setDescription('The index to insert the tile at')
                  .setRequired(true)
              )
          )
          .addSubcommand((builder) =>
            builder
              .setName('delete')
              .setDescription('Deletes a tile')
              .addIntegerOption((option) =>
                option
                  .setName('tile-number')
                  .setDescription('The number of the tile to delete. The first tile is number 1.')
                  .setRequired(true)
              )
          )
          .addSubcommand((builder) =>
            builder
              .setName('import')
              .setDescription('Imports a json tile')
              .addStringOption((option) =>
                option
                  .setName('tile-data')
                  .setDescription('The json data representing the tile')
                  .setRequired(true)
              )
              .addIntegerOption((option) =>
                option
                  .setName('tile-number')
                  .setDescription('Where to put the tile after import. This will shift all tiles after it to new numbers.')
                  .setRequired(false)
              )
          )
          .addSubcommand((builder) =>
            builder
              .setName('export')
              .setDescription('Returns the json representation for a tile')
              .addIntegerOption((option) =>
                option
                  .setName('tile-number')
                  .setDescription('The number of the tile to export. The first tile is number 1.')
                  .setRequired(true)
              )
          )
          .addSubcommand((builder) =>
            builder
              .setName('set-goto')
              .setDescription('Changes the tile that Snakes and Ladders go to')
              .addIntegerOption((option) =>
                option
                  .setName('tile-number')
                  .setDescription('The number of the tile to change. The first tile is number 1.')
                  .setRequired(true)
              )
              .addIntegerOption((option) =>
                option
                  .setName('goto')
                  .setDescription('The number of the tile to go to. The first tile is number 1.')
                  .setRequired(true)
              )
          )
      )
  }

}

export {TileAdminHandler}
