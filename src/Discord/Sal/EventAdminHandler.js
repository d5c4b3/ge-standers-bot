import {ChannelType} from "discord-api-types/v10";
import {container} from "@sapphire/framework";
import {SnakesAndLadders} from "../../SnakesAndLadders/SnakesAndLadders.js";
import {Board} from "../../SnakesAndLadders/Board.js";
import {Event} from "../../SnakesAndLadders/Event.js";
import {Util} from "./Util.js";

/**
 * Handle Chat Input Subcommands relating to the Event class
 */
class EventAdminHandler {

  /**
   * @param {ChatInputCommandInteraction} interaction
   * @return {Promise<*>}
   */
  async chatInputCreate(interaction) {
    await Util.ensureNoEvent(interaction);

    const repo = container.EventRepository;
    const guild_id = interaction.guildId.toString();
    const name = interaction.options.getString('event-name', true);
    const snakes_and_ladders = new SnakesAndLadders(name, [], new Board([]));
    const watch_channel = interaction.options.getChannel('watch-channel', true).id;
    const message_channel = interaction.options.getChannel('message-channel', true).id;
    const event = new Event(guild_id, snakes_and_ladders, watch_channel, message_channel);
    await repo.store(event);

    return interaction.reply({
      content: `Your event, ${snakes_and_ladders.name}, has been created`
    });

  }

  async chatInputCreateJson(interaction) {
    await Util.ensureNoEvent(interaction);

    const repo = container.EventRepository;
    const guild_id = interaction.guildId.toString();
    const snakes_and_ladders = repo.createSalFromJson(interaction.options.getString('event-json', true));
    const watch_channel = interaction.options.getChannel('watch-channel', true).id;
    const message_channel = interaction.options.getChannel('message-channel', true).id;
    const event = new Event(guild_id, snakes_and_ladders, watch_channel, message_channel);
    await repo.store(event);

    return interaction.reply({
      content: `Your event, ${snakes_and_ladders.name}, has been created`
    });
  }

  async chatInputSetWatchChannel(interaction, context) {
    const repo = container.EventRepository;
    const event = await Util.getEvent(interaction);
    event.watch_channel = interaction.options.getChannel('watch-channel', true).id;
    //TODO: Update listener?
    await repo.store(event);
    return interaction.reply({
      content: 'Updated watch channel'
    });
  }

  async chatInputSetMessageChannel(interaction, context) {
    const repo = container.EventRepository;
    const event = await Util.getEvent(interaction);
    event.message_channel = interaction.options.getChannel('message-channel', true).id;
    await repo.store(event);
    return interaction.reply({
      content: 'Updated message channel'
    });
  }

  async chatInputDeleteEvent(interaction, context) {
    //TODO: Error when no event exists?
    const repo = container.EventRepository;
    const guild_id = interaction.guildId.toString();
    repo.delete(guild_id);
    return interaction.reply({
      content: `Event removed`
    });
  }

  getSubcommandMappings() {
    return [
      {
        name: 'event',
        type: 'group',
        entries: [
          {
            name: 'create',
            chatInputRun: this.chatInputCreate.bind(this),
          },
          {
            name: 'create-json',
            chatInputRun: this.chatInputCreateJson.bind(this),
          },
          {
            name: 'set-watch-channel',
            chatInputRun: this.chatInputSetWatchChannel.bind(this),
          },
          {
            name: 'set-message-channel',
            chatInputRun: this.chatInputSetMessageChannel.bind(this),
          },
          {
            name: 'delete-event',
            chatInputRun: this.chatInputDeleteEvent.bind(this),
          },
        ]
      },
    ];
  }

  registerSubcommands(builder) {
    builder
      .addSubcommandGroup(builder =>
        builder
          .setName('event').setDescription('Event commands')
          .addSubcommand(builder =>
            builder
              .setName('create')
              .setDescription('Create a blank Snakes and Ladders event')
              .addStringOption(option =>
                option
                  .setName('event-name')
                  .setDescription('The name of the event')
                  .setRequired(true)
              )
              .addChannelOption(option =>
                option
                  .setName('watch-channel')
                  .setDescription('The channel to watch for drops')
                  .setRequired(true)
                  .addChannelTypes(ChannelType.GuildText)
              )
              .addChannelOption((option) =>
                option
                  .setName('message-channel')
                  .setDescription('The channel where update messages should be sent')
                  .setRequired(true)
                  .addChannelTypes(ChannelType.GuildText)
              )
          )
          .addSubcommand((builder) =>
            builder
              .setName('create-from-json')
              .setDescription('Create a new Snakes and Ladders event from json data')
              .addChannelOption((option) =>
                option
                  .setName('watch-channel')
                  .setDescription('The channel to watch for drops')
                  .setRequired(true)
                  .addChannelTypes(ChannelType.GuildText)
              )
              .addChannelOption((option) =>
                option
                  .setName('message-channel')
                  .setDescription('The channel where update messages should be sent')
                  .setRequired(true)
                  .addChannelTypes(ChannelType.GuildText)
              )
              .addStringOption((option) =>
                option
                  .setName('event-json')
                  .setDescription('The json representing the event')
                  .setRequired(true)
              )
          )
          .addSubcommand((builder) =>
            builder
              .setName('set-watch-channel')
              .setDescription('Sets the watch channel for the current event')
              .addChannelOption((option) =>
                option
                  .setName('watch-channel')
                  .setDescription('The channel to watch for drops')
                  .setRequired(true)
                  .addChannelTypes(ChannelType.GuildText)
              )
          )
          .addSubcommand((builder) =>
            builder
              .setName('set-message-channel')
              .setDescription('Sets the message channel for the current event')
              .addChannelOption((option) =>
                option
                  .setName('message-channel')
                  .setDescription('The channel where update messages should be sent')
                  .setRequired(true)
                  .addChannelTypes(ChannelType.GuildText)
              )
          )
          .addSubcommand((builder) =>
            builder
              .setName('delete-event')
              .setDescription('Deletes the current event')
          )
      )
  }
}

export {EventAdminHandler}
