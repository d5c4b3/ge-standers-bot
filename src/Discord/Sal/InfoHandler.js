import {Util} from "./Util.js";
import {TeamAdminHandler} from "./TeamAdminHandler.js";
import {DisplayableError} from "../Error/DisplayableError.js";
import {Timestamp} from "../Timestamp.js";

class InfoHandler {

  async chatInputTileInfo(interaction) {
    const event = await Util.getEvent(interaction);
    const tile_index = interaction.options.getInteger('tile-number', true);
    const tile = event.snakes_and_ladders.board.getTile(Util.fromInputIndex(tile_index));

    if (!tile)
      throw new DisplayableError(`Can't find tile ${tile_index}`);

    interaction.reply({
      content: `**${tile.name}**\n${tile.description}`,
    });
  }

  async chatInputTeamInfo(interaction) {
    const event = await Util.getEvent(interaction);

    const team_name = interaction.options.getString('team', false);

    if (team_name) {
      const team = TeamAdminHandler.getTeam(interaction, event, team_name);
      interaction.reply(InfoHandler.teamToString(event, team));
    } else {
      const teams = event.snakes_and_ladders.teams.map((team) => InfoHandler.teamToString(event, team)).join('\n');
      interaction.reply({
        content: teams
      });
    }
  }

  /**
   * @param {Event} event
   * @param {Team} team
   */
  static teamToString(event, team) {
    let lines = [];
    lines.push(`## ${team.name}`);

    // Add Tile Info
    const tile = event.snakes_and_ladders.board.getTile(team.current_tile_index);
    if (tile) {
      lines.push(`Tile: [${Util.toDisplayIndex(tile.index)}] ${tile.name}`);
    } else {
      lines.push(`Tile: ???`);
    }

    // Add reroll timestamp
    if (team.roll_time) {
      lines.push(`Last Roll: ${new Timestamp(team.roll_time, Timestamp.RELATIVE)}`);
    } else {
      lines.push("Last Roll: Never");
    }

    // Add Players
    for (const player of team.players) {
      lines.push(`- ${player}`);
    }

    return lines.join('\n');
  }

  getSubcommandMappings() {
    return [
      {
        name: 'team-info',
        chatInputRun: this.chatInputTeamInfo.bind(this),
      },
      {
        name: 'tile-info',
        chatInputRun: this.chatInputTileInfo.bind(this),
      },
    ];
  }

  registerSubcommands(builder) {
    builder
      .addSubcommand((builder) =>
        builder
          .setName('team-info')
          .setDescription('Displays the all teams or info about a specific team')
          .addStringOption(option =>
            option
              .setName('team')
              .setDescription('Optional. The name of a team to view')
              .setRequired(false)
              .setAutocomplete(true)
          )
      )
      .addSubcommand((builder) =>
        builder
          .setName('tile-info')
          .setDescription('Displays info about a tile')
          .addIntegerOption((option) =>
            option
              .setName('tile-number')
              .setDescription('The number of the tile to view. The first tile is number 1.')
              .setRequired(true)
          )
      )
  }
}

export {InfoHandler}
