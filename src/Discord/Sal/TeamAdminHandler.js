import {Util} from "./Util.js";
import {ChannelType} from "discord-api-types/v10";
import {Team} from "../../SnakesAndLadders/Team.js";
import {DisplayableError} from "../Error/DisplayableError.js";
import {CommonMessages} from "./CommonMessages.js";

class TeamAdminHandler {

  async chatInputCreate(interaction) {
    const event = await Util.getEvent(interaction);
    const team = new Team(interaction.options.getString('team-name', true), []);
    event.snakes_and_ladders.addTeam(team);
    await Util.persistEvent(interaction, event);

    interaction.reply({
      content: `New team, ${team.name}, created`
    });
  }

  async chatInputDelete(interaction) {
    const event = await Util.getEvent(interaction);
    const team_name = interaction.options.getString('team', true);
    // Just use this to throw an error if the team doesn't exist
    const team = TeamAdminHandler.getTeam(interaction, event, team_name);
    event.snakes_and_ladders.removeTeam(team_name);
    await Util.persistEvent(interaction, event);
    interaction.reply({
      content: `Team ${team.name} deleted`
    });
  }

  async chatInputSetTile(interaction) {
    const event = await Util.getEvent(interaction);
    const team_name = interaction.options.getString('team', true);
    const team = TeamAdminHandler.getTeam(interaction, event, team_name);
    let tile = interaction.options.getInteger('tile', true);

    if (tile >= event.snakes_and_ladders.board.tiles.length || tile < 0)
      throw new DisplayableError('Tile select is out of bounds');

    tile = Util.fromInputIndex(tile);

    team.previous_tile_index = null;
    team.current_tile_index = tile;

    await Util.persistEvent(interaction, event);
    interaction.reply({
      content: `Team updated`
    });
  }

  async chatInputAddPlayer(interaction) {
    const event = await Util.getEvent(interaction);
    const team_name = interaction.options.getString('team', true);
    const player = interaction.options.getString('player', true).trim();

    const team = TeamAdminHandler.getTeam(interaction, event, team_name);
    if (team.players.includes(player))
      throw new DisplayableError('That player already belongs to that team');

    team.players.push(player);
    await Util.persistEvent(interaction, event);
    interaction.reply({
      content: `Player added to team ${team.name}`
    });
  }

  async chatInputRemovePlayer(interaction) {
    const event = await Util.getEvent(interaction);
    const team_name = interaction.options.getString('team', true);
    const player = interaction.options.getString('player', true).trim();

    const team = TeamAdminHandler.getTeam(interaction, event, team_name);
    const index = team.players.indexOf(player);

    if (index === -1)
      throw new DisplayableError('Player is not a member of that team');

    team.players.splice(index, 1);

    await Util.persistEvent(interaction, event);
    interaction.reply({
      content: `Player removed from team ${team.name}`
    });
  }

  async chatInputAddDrop(interaction) {
    const event = await Util.getEvent(interaction);
    const team_name = interaction.options.getString('team', true);
    const drop = interaction.options.getString('drop', true).trim();

    const team = TeamAdminHandler.getTeam(interaction, event, team_name);
    const tile = event.snakes_and_ladders.board.getTile(team.current_tile_index);

    if (!tile) {
      interaction.reply({
        content: `${team.name} is on an invalid tile`,
      });
      return;
    }

    if (!tile.condition.isRelevant(drop)) {
      interaction.reply({
        content: `That drop isn't relevant for the current tile: [${Util.toDisplayIndex(tile.index)}] ${tile.name}`,
      });
      return;
    }

    team.addDrop(drop);

    if (tile.condition.isFulfilled(team.drops)) {
      const roll = event.snakes_and_ladders.rollTurn(team);

      // Save event after update
      await Util.persistEvent(interaction, event);

      await Util.sendRollResultMessage(interaction, event, team, roll)
    }

    interaction.reply({
      content: `${team.name} got the drop ${drop}`
    });
  }

  static getTeam(interaction, event, team_name) {
    const team = event.snakes_and_ladders.getTeam(team_name)
    if (team) return team;

    throw new DisplayableError('Could not find a team with that name');
  }

  getSubcommandMappings() {
    return [
      {
        name: 'team',
        type: 'group',
        entries: [
          {
            name: 'create',
            chatInputRun: this.chatInputCreate.bind(this),
          },
          {
            name: 'delete',
            chatInputRun: this.chatInputDelete.bind(this),
          },
          {
            name: 'add-player',
            chatInputRun: this.chatInputAddPlayer.bind(this),
          },
          {
            name: 'remove-player',
            chatInputRun: this.chatInputRemovePlayer.bind(this),
          },
          {
            name: 'set-tile',
            chatInputRun: this.chatInputSetTile.bind(this),
          },
          {
            name: 'add-drop',
            chatInputRun: this.chatInputAddDrop.bind(this),
          },
        ]
      },
    ];
  }

  registerSubcommands(builder) {
    builder
      .addSubcommandGroup(builder =>
        builder
          .setName('team').setDescription('Team commands')
          .addSubcommand(builder =>
            builder
              .setName('create')
              .setDescription('Create a new team')
              .addStringOption(option =>
                option
                  .setName('team-name')
                  .setDescription('The name of the team')
                  .setRequired(true)
              )
          )
          .addSubcommand((builder) =>
            builder
              .setName('delete')
              .setDescription('Deletes a team')
              .addStringOption(option =>
                option
                  .setName('team')
                  .setDescription('The name of the team to delete')
                  .setRequired(true)
                  .setAutocomplete(true)
              )
          )
          .addSubcommand((builder) =>
            builder
              .setName('set-tile')
              .setDescription('Moves the team to the specified tile')
              .addStringOption(option =>
                option
                  .setName('team')
                  .setDescription('The name of the team to move')
                  .setRequired(true)
                  .setAutocomplete(true)
              )
              .addIntegerOption((option) =>
                option
                  .setName('tile')
                  .setDescription('The index of the tile')
                  .setRequired(true)
              )
          )
          .addSubcommand((builder) =>
            builder
              .setName('add-player')
              .setDescription('Adds a player to a team')
              .addStringOption(option =>
                option
                  .setName('team')
                  .setDescription('The name of the team to add the player to')
                  .setRequired(true)
                  .setAutocomplete(true)
              )
              .addStringOption(option =>
                option
                  .setName('player')
                  .setDescription('The name of the player')
                  .setRequired(true)
              )
          )
          .addSubcommand((builder) =>
            builder
              .setName('remove-player')
              .setDescription('Removes a player from a team')
              .addStringOption(option =>
                option
                  .setName('team')
                  .setDescription('The name of the team the player belongs to')
                  .setRequired(true)
                  .setAutocomplete(true)
              )
              .addStringOption(option =>
                option
                  .setName('player')
                  .setDescription('The name of the player')
                  .setRequired(true)
                  .setAutocomplete(true)
              )
          )
          .addSubcommand((builder) =>
            builder
              .setName('add-drop')
              .setDescription('Adds a drop to the team')
              .addStringOption(option =>
                option
                  .setName('team')
                  .setDescription('The name of the team to add the drop to')
                  .setRequired(true)
                  .setAutocomplete(true)
              )
              .addStringOption(option =>
                option
                  .setName('drop')
                  .setDescription('The drop the team should get')
                  .setRequired(true)
              )
          )
      )
  }
}

export {TeamAdminHandler}
