import {container} from "@sapphire/framework";
import {DisplayableError} from "../Error/DisplayableError.js";
import {CommonMessages} from "./CommonMessages.js";

class Util {
  /**
   * @param {int} tile_index
   * @return {int}
   */
  static fromInputIndex(tile_index) {
    return tile_index - 1;
  }

  /**
   * @param {int} tile_index
   * @return {int}
   */
  static toDisplayIndex(tile_index) {
    return tile_index + 1;
  }

  static async ensureNoEvent(interaction) {
    const repo = container.EventRepository;
    const guild_id = interaction.guildId.toString();
    const event = await repo.findOneByGuild(guild_id);

    if (event) {
      throw new DisplayableError(`An event already exists for this server, ${event.snakes_and_ladders.name}`);
    }
  }

  static async getEvent(interaction) {
    const repo = container.EventRepository;
    const guild_id = interaction.guildId.toString();
    const event = await repo.findOneByGuild(guild_id);

    if (!event) {
      throw new DisplayableError('No event found, please create one with `/sal event create`');
    }

    return event;
  }

  static async persistEvent(interaction, event) {
    /** @type {EventRepository} */
    const repo = container.EventRepository;
    await repo.store(event);
  }

  /**
   * Returns a display string for the tile
   * @param {Tile} tile
   * @return {string}
   */
  static displayTile(tile) {
    return `[${Util.toDisplayIndex(tile.index)}] **${tile.name}**\n${tile.description}`;
  }

  static async sendRollResultMessage(interaction, event, team, roll, got_the_drop = true) {
    const roll_message = CommonMessages.buildRollMessage(event, team, roll);

    let msg = '';
    if (roll.is_reroll) {
      msg = `# 🎲 ${team.name} used their re-roll! 🎲\n`;
    } else if (got_the_drop) {
      msg = `# 🎉 ${team.name} got the drop! 🎉 \n`;
    } else {
      msg = `# 🎲 ${team.name} is rolling the dice! 🎲\n`;
    }

    msg += `${roll_message}\n\n Your new objective is:\n` + Util.displayTile(roll.tile);
    await interaction.client.channels.cache.get(event.message_channel).send(msg);
  }
}

export {Util}
