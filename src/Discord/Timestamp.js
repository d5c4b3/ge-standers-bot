class Timestamp {
  static get SHORT_TIME() { return 't' }
  static get LONG_TIME() { return 'T' }
  static get SHORT_DATE() { return 'd' }
  static get LONG_DATE() { return 'D' }
  static get SHORT_DATE_TIME() { return 'f' }
  static get LONG_DATE_TIME() { return 'F' }
  static get RELATIVE() { return 'R' }


  constructor(time, format = null) {
    if (time instanceof Date) {
      time = Math.floor(time.getTime() / 1000);
    }
    this.time = time;
    this.format = format;
  }

  toString() {
    let time = `t:${this.time}:`;
    if (this.format) time += this.format;
    return `<${time}>`;
  }
}

export {Timestamp}
