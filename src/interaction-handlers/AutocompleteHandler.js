import {InteractionHandler, InteractionHandlerTypes, container} from "@sapphire/framework";

class AutocompleteHandler extends InteractionHandler {
  constructor(ctx, options) {
    super(ctx, {
      ...options,
      interactionHandlerType: InteractionHandlerTypes.Autocomplete
    });
  }

  async run(interaction, result) {
    return interaction.respond(result);
  }

  async parse(interaction) {
    const subcommand = interaction.options.getSubcommand();
    const group = interaction.options.getSubcommandGroup();

    // TODO: filter this better
    if (
      subcommand !== 'roll' &&
      subcommand !== 'reroll' &&
      subcommand !== 'team-info' && // Why didn't this break when missing?
      (group === 'team' && subcommand !== 'list') &&
      (group === 'team' && subcommand !== 'delete') &&
      (group === 'team' && subcommand !== 'add-player') &&
      (group === 'team' && subcommand !== 'add-drop') &&
      (group === 'team' && subcommand !== 'remove-player') &&
      (group === 'team' && subcommand !== 'set-tile')
    ) return this.none();

    const focusedOption = interaction.options.getFocused(true);

    switch (focusedOption.name) {
      case 'team': {
        const repo = container.EventRepository;
        const guild_id = interaction.guildId.toString();
        const event = await repo.findOneByGuild(guild_id);
        if (!event) return this.none();
        return this.some(event.snakes_and_ladders.teams.map((team) => ({name: team.name, value: team.name})));
      }
      case 'player': {
        const repo = container.EventRepository;
        const guild_id = interaction.guildId.toString();
        const event = await repo.findOneByGuild(guild_id);
        if (!event) return this.none();
        const team_option = interaction.options.get('team');
        if (!team_option) return this.none();
        const team = event.snakes_and_ladders.getTeam(team_option.value);
        if (!team) return this.none();
        if (team.players.length > 0) return this.some(team.players.map(player => ({name: player, value: player})));
        return this.none();
      }
      default:
        this.none();
    }
  }
}

export {AutocompleteHandler};
