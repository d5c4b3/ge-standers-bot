import {SnakesAndLadders} from "../SnakesAndLadders/SnakesAndLadders.js";
import {Event} from "../SnakesAndLadders/Event.js";
import {Team} from "../SnakesAndLadders/Team.js";
import {Board} from "../SnakesAndLadders/Board.js";
import {SalSerializer} from "../Serializer/SnakesAndLadders/SalSerializer.js";

class EventRepository {

  /**
   * @param {Database} db
   * @param {Cache} cache
   */
  constructor(db, cache) {
    this.db = db;
    this.cache = cache;
    this.salSerializer = new SalSerializer();
  }

  /**
   * @param {string} guild_id
   * @returns {Promise<Event|null>}
   */
  async findOneByGuild(guild_id) {
    const cached = this.cache.get(guild_id);
    if (cached) return cached;

    const stmt = this.db.prepare('select watch_channel, message_channel, data from snakes_and_ladders where guild_id = ?');
    const row = stmt.get(guild_id);
    if (row === undefined) return null;

    const event = new Event(guild_id, this.salSerializer.parse(row.data), row.watch_channel, row.message_channel);
    this.cache.set(guild_id, event, [row.watch_channel]);
    return event;
  }

  /**
   * @param {string} channel_id
   * @returns {Promise<Event|null>}
   */
  async findOneByWatchChannel(channel_id) {
    const cached = this.cache.get(channel_id);
    if (cached) return cached;

    const stmt = this.db.prepare('select guild_id, message_channel, data from snakes_and_ladders where watch_channel = ?');
    const row = stmt.get(channel_id);
    if (row === undefined) return null;

    const event = new Event(row.guild_id, this.salSerializer.parse(row.data), channel_id, row.message_channel);
    this.cache.set(row.guild_id, event, [channel_id]);
    return event;
  }

  /**
   * @param {Event} event
   * @return void
   */
  async store(event) {
    // Update cache
    this.cache.set(event.guild_id, event, [event.watch_channel]);

    // Update database
    const stmt = this.db.prepare('insert into snakes_and_ladders (guild_id, watch_channel, message_channel, data) values ($guild_id, $watch_channel, $message_channel, $data) on conflict(guild_id) do update set watch_channel=$watch_channel, message_channel=$message_channel, data=$data');
    stmt.run({
      guild_id: event.guild_id,
      watch_channel: event.watch_channel,
      message_channel: event.message_channel,
      data: this.salSerializer.serialize(event.snakes_and_ladders)
    });
  }

  async delete(guild_id) {
    this.cache.remove(guild_id);

    const stmt = this.db.prepare('delete from snakes_and_ladders where guild_id = ?');
    stmt.run(guild_id);
  }

  /**
   * Creates a SnakesAndLadders from an external json source
   * @param {string} json
   * @return {SnakesAndLadders}
   */
  createSalFromJson(json) {
    throw new Error('Import structure is not defined for new Tile and Condition classes');
    const data = JSON.parse(json);
    const name = data.name;
    const teams = [];
    for (const team of data.teams) {
      const new_team = new Team(team.name, team.players, team.current_tile);
      teams.push(new_team);
    }

    const snakes = [];
    const ladders = [];
    for (let i = 0; i < data.board.tiles.length; i++) {
      if ('go_to' in data.board.tiles[i]) {
        if (i > data.board.tiles[i].go_to) {
          snakes.push([i, data.board.tiles[i].go_to]);
        } else {
          ladders.push([data.board.tiles[i].go_to, i]);
        }
      }
    }

    const board = new Board(data.board.tiles, snakes, ladders);

    return new SnakesAndLadders(name, teams, board);
  }
}

export { EventRepository }
