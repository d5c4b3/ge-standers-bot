import {DropCondition} from "./DropCondition.js";

class AndCondition extends DropCondition {
  /**
   * @param {DropCondition[]} conditions
   */
  constructor(conditions) {
    super();
    this.conditions = conditions;
  }

  /**
   * @inheritDoc
   */
  isFulfilled(drops) {
    for (const condition of this.conditions) {
      if (!condition.isFulfilled(drops)) return false;
    }

    return true;
  }

  isRelevant(drop) {
    for (const condition of this.conditions) {
      if (condition.isRelevant(drop)) return true;
    }

    return false;
  }
}

export {AndCondition}
