import {DropCondition} from "./DropCondition.js";

class AlwaysFalseCondition extends DropCondition {}

export {AlwaysFalseCondition}
