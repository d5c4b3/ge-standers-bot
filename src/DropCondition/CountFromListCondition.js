import {DropCondition} from "./DropCondition.js";

class CountFromListCondition extends DropCondition {

  /**
   * @param {{drop: string, count: int}[]} list
   */
  constructor(list) {
    super();

    this.list = list;
  }

  /**
   * @inheritDoc
   */
  isFulfilled(drops) {
    let counts = [];
    for (let i = 0; i < this.list.length; i++) {
      counts[i] = 0;
    }

    for (const drop of drops) {
      for (let i = 0; i < this.list.length; i++) {
        if (!this.areDropsSame(this.list[i].drop, drop)) continue;

        // This is a drop we care about, increase the count for this index
        counts[i] ++;

        // Check if we've got enough of those drops
        if (counts[i] >= this.list[i].count) {
          return true;
        }

        break;
      }
    }

    return false;
  }

  isRelevant(drop) {
    for (const item of this.list) {
      if (this.areDropsSame(drop, item.drop)) return true;
    }

    return false;
  }
}
export {CountFromListCondition}
