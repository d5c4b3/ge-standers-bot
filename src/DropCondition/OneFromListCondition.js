import {DropCondition} from "./DropCondition.js";

class OneFromListCondition extends DropCondition {

  /**
   * @param {[]} list
   */
  constructor(list) {
    super();

    this.list = list;
  }


  /**
   * @inheritDoc
   */
  isFulfilled(drops) {
    for (const drop of drops) {
      for (const item of this.list) {
        if (this.areDropsSame(drop, item)) return true;
      }
    }

    return false;
  }

  isRelevant(drop) {
    for (const item of this.list) {
      if (this.areDropsSame(drop, item)) return true;
    }

    return false;
  }
}

export {OneFromListCondition}
