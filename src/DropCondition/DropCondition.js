class DropCondition {

  /**
   * Determines if a list of item drops fulfills the condition.
   *
   * @param {[]} drops A list of drops received.
   * Example:
   * [
   *   "Ahrim's hood",
   *   "Verac's brassard",
   *   "Ahrim's hood",
   * ]
   * @return {boolean}
   */
  isFulfilled(drops) {
    return false;
  }

  /**
   * Returns true if the drop could affect the condition's fulfillment
   * @param drop
   */
  isRelevant(drop) {
    return false;
  }

  /**
   * Returns true if two drops are the same item
   * @param {string} drop1
   * @param {string} drop2
   * @return {boolean}
   */
  areDropsSame(drop1, drop2) {
    return (drop1.trim().toLowerCase() === drop2.trim().toLowerCase());
  }

}

export {DropCondition}
