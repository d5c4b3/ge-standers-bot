import {DropCondition} from "./DropCondition.js";

class OrCondition extends DropCondition {
  /**
   * @param {DropCondition[]} conditions
   */
  constructor(conditions) {
    super();
    this.conditions = conditions;
  }

  isFulfilled(drops) {
    for (const condition of this.conditions) {
      if (condition.isFulfilled(drops)) return true;
    }

    return false;
  }

  isRelevant(drop) {
    for (const condition of this.conditions) {
      if (condition.isRelevant(drop)) return true;
    }

    return false;
  }
}

export {OrCondition}
