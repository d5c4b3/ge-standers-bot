import {DropCondition} from "./DropCondition.js";

class SlottedSetCondition extends DropCondition {

  /**
   * @param {{name: string, drops: any[]}[]} slots
   */
  constructor(slots) {
    super();

    this.slots = slots;
  }

  #dropInSet(set, drop) {
    for (const item of set) {
      if (this.areDropsSame(item, drop)) return true;
    }

    return false;
  }

  isFulfilled(drops) {
    // TODO: how does this work if there is a drop that can belong in multiple slots?
    const collected_drops = new Map();

    for (const drop of drops) {
      // for name, items of [{helm, [...]}, {chest, [...]},]
      for (const slot of this.slots) {
        if (!this.#dropInSet(slot.drops, drop)) continue;

        // We've already got a drop for this slot
        // Eg, We've already got a helm
        if (collected_drops.has(slot.name)) continue;

        // Store the item in its slot
        collected_drops.set(slot.name, drop);

        // Check if we have a drop for each slot
        if (collected_drops.size === this.slots.length) {
          return true;
        }
      }
    }

    return false;
  }

  isRelevant(drop) {
    for (const slot of this.slots) {
      if (this.#dropInSet(slot.drops, drop)) return true;
    }

    return false;
  }

}

export {SlottedSetCondition}
