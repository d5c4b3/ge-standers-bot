import {DropCondition} from "./DropCondition.js";

class SetCondition extends DropCondition {

  /**
   * @param {[][]} sets
   */
  constructor(sets) {
    super();
    this.sets = sets;
  }

  #dropInSet(set, drop) {
    for (const item of set) {
      if (this.areDropsSame(item, drop)) return true;
    }

    return false;
  }

  isFulfilled(drops) {

    for (let i = 0; i < this.sets.length; i++) {
      // You can't complete a set if you don't have at
      // least as many drops as the set has items
      if (drops.length < this.sets[i].length) continue;

      // Copy the set so we can modify it
      let set = [...this.sets[i]];

      // Loop through the drops to see if we've completed the set
      for (let j = 0; j < drops.length; j++) {
        const index = set.findIndex((item) => {
          return this.areDropsSame(item, drops[j]);
        });

        if (index === -1) continue;

        // We found a drop, remove it from the set to mark it as collected
        set.splice(index, 1);

        // We've removed every item from a set so the condition is fulfilled
        if (set.length === 0) {
          return true;
        }
      }
    }

    return false;
  }

  isRelevant(drop) {
    for (const set of this.sets) {
      if (this.#dropInSet(set, drop)) return true;
    }

    return false;
  }
}

export {SetCondition}
