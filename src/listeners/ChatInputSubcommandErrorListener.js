import {Events, Listener} from "@sapphire/framework";
import {SubcommandPluginEvents} from "@sapphire/plugin-subcommands";
import {DisplayableError} from "../Discord/Error/DisplayableError.js";

class ChatInputSubcommandErrorListener extends Listener {
  constructor(context, options) {
    super(context, {
      ...options,
      event: SubcommandPluginEvents.ChatInputSubcommandError,
    });
  }

  async run(error, context) {
    if (error instanceof DisplayableError) {
      await context.interaction.reply({
        content: error.message
      });
      return;
    }

    console.error(error);
  }
}

export {ChatInputSubcommandErrorListener};
