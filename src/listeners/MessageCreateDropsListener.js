import {Events, Listener, container} from "@sapphire/framework";
import {Message} from "discord.js";
import {SalCommand} from "../commands/sal.js";
import {CommonMessages} from "../Discord/Sal/CommonMessages.js";
import {Util} from "../Discord/Sal/Util.js";

class MessageCreateDropsListener extends Listener {
  constructor(context, options) {
    super(context, {
      ...options,
      event: Events.MessageCreate,
    });
  }

  /**
   * @param {Message} message
   */
  async run(message) {
    // TODO: only check messages from specific webook?

    // Only check messages from webhooks
    if (!message.webhookId) return;
    if (message.author.id === message.client.user.id) return;

    // TODO: have debug set from environment variable
    return MessageCreateDropsListener.handleDropMessage(message, true);
  }

  /**
   * @param {Message} message
   * @param {boolean} debug
   */
  static async handleDropMessage(message, debug = false) {
    const repo = container.EventRepository;
    const event = await repo.findOneByWatchChannel(message.channel.id);
    if (!event) return;

    const parts = message.content.split(' - ');
    // Message doesn't fit format. TODO: log this for debugging
    if (parts.length < 4) {
      if (debug) console.log(parts);
      return;
    }

    // **Iron Evil478** - **no team** - **mind rune** - **0**
    // TODO: Should we force the input message to have bold?
    const name = MessageCreateDropsListener.trimBold(parts[0].trim());
    const team_name = MessageCreateDropsListener.trimBold(parts[1].trim());
    const drop = MessageCreateDropsListener.trimBold(parts[2].trim());
    const tile_i = parseInt(MessageCreateDropsListener.trimBold(parts[3].trim()));

    const team = event.snakes_and_ladders.getTeam(team_name);

    if (!team) {
      if (debug) console.info(`Found drop message (${message.id}), but not team "${team_name}"`);
      return;
    }

    const tile = event.snakes_and_ladders.board.getTile(team.current_tile_index);

    if (!tile) {
      console.warn(`Couldn't find tile at ${team.current_tile_index} for team`);
      return;
    }

    // We got *a* drop from a team member, but it's not relevant to the current
    // tile
    if (!tile.condition.isRelevant(drop)) {
      await message.react('⛔');
      return;
    }

    team.addDrop(drop);

    if (tile.condition.isFulfilled(team.drops)) {
      const roll = event.snakes_and_ladders.rollTurn(team);

      // Save event after update
      await repo.store(event);

      await Util.sendRollResultMessage(message, event, team, roll)
    } else {
      await message.react('✅');
    }
  }

  static trimBold(str) {
    return str.replace(/^\*+|\*+$/g, '');
  }
}

export {MessageCreateDropsListener};
