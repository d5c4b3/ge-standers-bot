/**
 * For discord related data/methods
 */
class Event {

  /**
   * @param {string} guild_id
   * @param {SnakesAndLadders} snakes_and_ladders
   * @param {string} watch_channel
   * @param {string} message_channel
   */
  constructor(guild_id, snakes_and_ladders, watch_channel, message_channel) {
    this.guild_id = guild_id;
    this.snakes_and_ladders = snakes_and_ladders;
    this.watch_channel = watch_channel;
    this.message_channel = message_channel;
  }
}

export {Event};
