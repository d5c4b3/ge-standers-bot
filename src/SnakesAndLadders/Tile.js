class Tile {

  //TODO: remove the index reference, it doesn't make sense and is hard to maintain

  /**
   * @param {int} index
   * @param {string} name
   * @param {string} description
   * @param {DropCondition|null} condition
   * @param {int|null} goto
   */
  constructor(index, name, description, condition = null, goto = null) {
    this.index = index;
    this.name = name;
    this.description = description;

    if (condition === null && goto === null) {
      throw new Error('One of condition or goto must be set');
    }

    this.condition = condition;
    this.goto = goto;
  }


  isSnake() {
    if (this.goto === null) return false;

    return this.goto < this.index;
  }

  isLadder() {
    if (this.goto === null) return false;

    return this.goto > this.index;
  }

}

export {Tile}
