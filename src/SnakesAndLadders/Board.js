class Board {

  /**
   * @param {Tile[]} tiles
   */
  constructor(tiles) {
    this.tiles = tiles;
  }

  /**
   * @param {int} index
   * @return {Tile|null}
   */
  getTile(index) {
    if (index < 0 || index >= this.tiles.length) return null;
    return this.tiles[index];
  }

  moveTile(tile, to_index) {
    const from_index = tile.index;

    if (to_index > this.tiles.length) {
      to_index = this.tiles.length;
    }
    if (to_index < 1) {
      to_index = 0;
    }
    if (to_index === from_index) return;

    this.tiles.splice(to_index, 0, this.tiles.splice(from_index, 1)[0]);

    for (let i = 0; i < this.tiles.length; i++) {
      // Reset tile indexes
      this.tiles[i].index = i;

      // If goto is between to and from then we need to either shift it
      // up or down depending on if the tile is moving backwards or forwards
      if (this.tiles[i].goto !== null) {
        const goto = this.tiles[i].goto;
        if (goto > from_index && goto < to_index) {
          this.tiles[i].goto --;
        } else if (goto < from_index && goto > to_index) {
          this.tiles[i].goto ++;
        }
      }
    }
  }

  insertTile(index, tile) {
    //Insert the tile
    this.tiles = this.tiles.toSpliced(index, 0, tile);

    // If we were just updating indexes we could start at index
    // But we also have to update goto
    for (let i = 0; i < this.tiles.length; i++) {
      // Update the tile index
      this.tiles[i].index = i;

      // Update the goto if necessary
      // Don't update gotos that point past the end of the board (it messes with people sequentially adding tiles)
      if (this.tiles[i].goto !== null && this.tiles[i].goto < this.tiles.length && this.tiles[i].goto >= index) {
        this.tiles[i].goto ++;
      }
    }
  }

  deleteTile(index) {
    this.tiles = this.tiles.toSpliced(index, 1);

    for (let i = 0; i < this.tiles.length; i++) {
      this.tiles[i].index = i;

      if (this.tiles[i].goto > index) {
        this.tiles[i].goto --;
      }
    }
  }

  /**
   * If the given tile is the start of a snake, return the index of the connected tile,
   * otherwise return null.
   * @param {number} index
   * @returns {number|null}
   */
  getTileSnake(index) {
    const tile = this.getTile(index);
    return tile.isSnake() ? tile.goto : null;
  }

  /**
   * If the given tile is the start of a ladder, return the index of the connected tile,
   * otherwise return null.
   * @param {number} index
   * @returns {number|null}
   */
  getTileLadder(index) {
    const tile = this.getTile(index);
    return tile.isLadder() ? tile.goto : null;
  }
}

export {Board};
