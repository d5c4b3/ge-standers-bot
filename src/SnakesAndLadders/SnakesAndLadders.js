class SnakesAndLadders {

  /**
   * @param {string} name
   * @param {Team[]} teams
   * @param {Board} board
   */
  constructor(name, teams, board) {
    this.name = name;
    this.teams = teams;
    this.board = board;
  }

  /**
   * Get a team by team name
   * @param {string} name
   * @return {Team|null}
   */
  getTeam(name) {
    const lower_name = name.trim().toLowerCase();
    for (const team of this.teams) {
      if (team.name.trim().toLowerCase() === lower_name) return team;
    }
    return null;
  }

  /**
   * @param {Team} team
   */
  addTeam(team) {
    this.teams.push(team);
  }

  /**
   * @param {string} name
   */
  removeTeam(name) {
    const lower_name = name.trim().toLowerCase();
    for (let i = 0; i < this.teams.length; i++) {
      if (this.teams[i].name.trim().toLowerCase() === lower_name) {
        this.teams.splice(i, 1);
        break;
      }
    }
  }

  /**
   * Insert the tile into the board at the specified location, updates the team
   * locations, so they are consistent.
   * @param index
   * @param tile
   */
  insertTile(index, tile) {
    if (index > this.board.tiles.length) {
      index = this.board.tiles.length;
    }

    if (index < 0) {
      index = 0;
    }

    this.board.insertTile(index, tile);

    // If index = 8 and team's tile >= 8 then we increment the team index
    for (const team of this.teams) {
      if (team.current_tile_index >= index) {
        team.current_tile_index++;
      }

      if (team.previous_tile_index >= index) {
        team.previous_tile_index++;
      }
    }
  }

  deleteTile(index) {
    this.board.deleteTile(index);

    // If index = 8 and team's tile > 8, decrement the tile
    // So team.index = 10 becomes team.index = 9
    for (const team of this.teams) {
      if (team.current_tile_index > index) {
        team.current_tile_index--;
      }

      if (team.previous_tile_index > index) {
        team.previous_tile_index--;
      }
    }
  }

  /**
   * @param {Team} team
   */
  rollTurn(team) {
    if (!this.teams.includes(team)) throw Error(`Team ${team.name} isn't a part of event ${this.name}`);

    const tile = this.board.getTile(team.current_tile_index);
    const rolls = this._rollDice(this._getDiceForTile(tile));
    let sum = rolls.reduce((sum, roll) => sum + roll, 0);
    let next_tile_i = team.current_tile_index + sum;

    if (next_tile_i >= this.board.tiles.length)
      next_tile_i = this.board.tiles.length - 1;

    let next_tile = this.board.getTile(next_tile_i);

    // Handle the case where we landed on a snake or ladder
    let snake_from_i = null;
    let ladder_from_i = null;

    if (next_tile.isSnake()) {
      snake_from_i = next_tile.index;
      next_tile_i = next_tile.goto;
    }

    if (next_tile.isLadder()) {
      ladder_from_i = next_tile.index;
      next_tile_i = next_tile.goto;
    }

    next_tile = this.board.getTile(next_tile_i);
    team.moveTile(next_tile_i);
    team.roll_time = new Date();

    // Should this be a defined "RollEvent" class?
    return {
      rolls: rolls,
      sum: sum,
      previous_index: team.previous_tile_index,
      tile_index: next_tile_i,
      tile: next_tile,
      snake_from_index: snake_from_i,
      ladder_from_index: ladder_from_i,
      is_reroll: false,
    }
  }

  /**
   * Team decided that they didn't like the way the roll went and wanted to
   * re-do it.
   * @param {Team} team
   */
  rerollTurn(team) {
    if (team.previous_tile_index === null) throw Error('Previous tile is null, can\'t reroll turn');
    if (!this.teams.includes(team)) throw Error(`Team ${team.name} isn't a part of event ${this.name}`);

    const reroll_from_i = team.current_tile_index;
    team.undoTileMove();

    const tile = this.board.getTile(team.current_tile_index);
    let next_tile_i, rolls, sum;

    // Try to make sure the next roll doesn't land them on the tile they
    // rerolled from. Because of snakes, ladders, and length trim there isn't
    // a good way to *ensure* no duplicate roll. Instead, we are just trying
    // and checking
    for (let i = 0; i < 20; i++) {
      rolls = this._rollDice(this._getDiceForTile(tile));
      sum = rolls.reduce((sum, roll) => sum + roll, 0);
      next_tile_i = team.current_tile_index + sum;

      if (next_tile_i >= this.board.tiles.length)
        next_tile_i = this.board.tiles.length - 1;

      if (next_tile_i !== reroll_from_i) break;
    }

    // TODO: this was copied from rollTurn. Refactor out to utility method
    let next_tile = this.board.getTile(next_tile_i);

    // Handle the case where we landed on a snake or ladder
    let snake_from_i = null;
    let ladder_from_i = null;

    if (next_tile.isSnake()) {
      snake_from_i = next_tile.index;
      next_tile_i = next_tile.goto;
    }

    if (next_tile.isLadder()) {
      ladder_from_i = next_tile.index;
      next_tile_i = next_tile.goto;
    }

    next_tile = this.board.getTile(next_tile_i);
    team.moveTile(next_tile_i);
    team.roll_time = new Date();

    // Should this be a defined "RollEvent" class?
    return {
      rolls: rolls,
      sum: sum,
      previous_index: team.previous_tile_index,
      tile_index: next_tile_i,
      tile: next_tile,
      snake_from_index: snake_from_i,
      ladder_from_index: ladder_from_i,
      is_reroll: true,
    }
  }

  /**
   * @param {int} tile_index
   * @return {int}
   */
  fromInputIndex(tile_index) {
    return tile_index - 1;
  }

  /**
   * @param {int} tile_index
   * @return {int}
   */
  toDisplayIndex(tile_index) {
    return tile_index + 1;
  }

  /**
   * Returns a dice array, which is an array of die arrays. A die array is a
   * list of numbers. To roll a dice array sum a random element from each array.
   * @param tile
   * @returns {number[][]}
   * @private
   * @see _rollDice
   */
  _getDiceForTile(tile) {
    if (tile.hasOwnProperty('dice')) return tile.dice;
    return [[1, 2, 3, 4]];
  }


  /**
   * Takes an array of die (integer arrays) and rolls each die. Output will be
   * an array of die rolls
   * @param {number[][]} dice
   * @returns {number[]}
   * @private
   */
  _rollDice(dice) {
    return dice.map((die) => die[Math.floor(Math.random() * die.length)]);
  }
}

export {SnakesAndLadders};
