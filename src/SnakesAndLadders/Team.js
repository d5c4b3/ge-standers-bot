class Team {

  /**
   * @param {string} name
   * @param {string[]} players
   * @param {number} current_tile_index
   * @param {number|null} previous_tile_index
   * @param {Date} roll_time
   */
  constructor(name, players, current_tile_index = 0, previous_tile_index = null, roll_time = null) {
    this.name = name;
    this.players = players;
    this.current_tile_index = current_tile_index;
    this.previous_tile_index = previous_tile_index;
    this.drops = [];
    this.roll_time = roll_time;
  }

  addDrop(drop) {
    this.drops.push(drop);
  }

  moveTile(index) {
    this.previous_tile_index = this.current_tile_index;
    this.current_tile_index = index;
    this.drops = [];
  }

  undoTileMove() {
    this.current_tile_index = this.previous_tile_index;
    this.previous_tile_index = null;
    this.drops = [];
  }
}

export {Team};
