import {createCanvas} from "canvas";

class SnakesAndLaddersRenderer {

  constructor(tile_size = 80) {
    this.tile_size = tile_size;
    this.colors = [
      '#00a6ed',
      '#ff4b4b',
      '#ffd900',
      '#a0e310',
      '#c954ff',
      '#f5701d',
    ];
  }

  render(sal) {
    // Number of rows and columns
    let rows = Math.max(Math.ceil(Math.sqrt(sal.board.tiles.length)), 4);
    const cols = rows;

    // Can we remove exactly one row?
    if ((rows * cols) - sal.board.tiles.length > cols) {
      rows --;
    }

    // Add height for header
    //TODO: do we need to set a min width to accompany header?
    const header_height = this.tile_size * 1.5;

    const width = cols * this.tile_size;
    const height = rows * this.tile_size + header_height;

    // tile index text margin for top & right
    const ti_margin = 5;
    const name_margin = 5;

    // Team indicator size and margin for bottom & left
    const team_indicator_r = 5;
    const team_indicator_margin = 3;
    const team_indicator_space = 3;

    // constants for header
    const header_margin = 5;
    const team_name_space = 10;
    const background_color = '#d5d5d5';

    const canvas = createCanvas(width, height);
    const ctx = canvas.getContext('2d');

    // Calculate font size data for tile index text
    ctx.textAlign = "right";
    ctx.fillStyle = '#000';
    ctx.font = '10px sans-serif';
    const tile_index_text_size = ctx.measureText('000');
    const ti_y_offset = tile_index_text_size.actualBoundingBoxAscent + ti_margin //top margin

    // Render header
    // Header background
    ctx.fillStyle = background_color;
    ctx.fillRect(0, 0, width, height);

    // Header Event Name
    ctx.textAlign = 'left';
    ctx.font = '15px sans-serif';
    ctx.fillStyle = '#000';
    const heading_size = ctx.measureText(sal.name);
    this.fillWrappedText(ctx, sal.name, header_margin, header_margin + heading_size.actualBoundingBoxAscent, width - header_margin*2, 2);

    // Header team legend
    this.giveTeamsColors(sal.teams);

    ctx.save()
    ctx.textAlign = 'left';
    ctx.font = '10px sans-serif';
    ctx.textBaseline = 'middle'
    const board_top = height - (this.tile_size * rows);
    let x = team_indicator_margin;
    let y = board_top - team_indicator_r - team_indicator_margin;
    const text_measure = ctx.measureText('a');
    const team_line_height = (text_measure.emHeightDescent + text_measure.emHeightAscent) * 1.2;
    for (const team of sal.teams) {
      const team_name_measure = ctx.measureText(team.name)
      let indicator_width = 2*team_indicator_space + 2*team_indicator_r;
      let team_width = indicator_width + team_name_measure.width;
      if (x + team_width > width) {
        y -= team_line_height;
        x = team_indicator_margin;
      }

      if (x !== team_indicator_margin) {
        x += team_name_space;
      }

      ctx.fillStyle = team.color;
      this.fillCircle(ctx, x + team_indicator_space + team_indicator_r, y, team_indicator_r);
      ctx.fillStyle = '#000';
      ctx.fillText(team.name, x + indicator_width, y);
      x += team_width;
    }
    ctx.restore();

    // Render empty tiles just so we display *something*
    for (let row = 0; row < rows; row++) {
      for (let col = 0; col < cols; col++) {
        // Render tile background
        const pos = this.getTileLocation(width, height, cols, rows, (row * cols) + col);
        ctx.fillStyle = background_color;
        ctx.fillRect(pos.x, pos.y, this.tile_size, this.tile_size);

        // Render tile border
        ctx.strokeStyle = '#8d8d8d';
        ctx.lineWidth = 2;
        ctx.strokeRect(pos.x, pos.y, this.tile_size, this.tile_size);
      }
    }

    // If there are no tiles only render the header
    if (sal.board.tiles.length === 0) return canvas;

    // Render tiles
    for (const tile of sal.board.tiles) {
      // Render tile background
      const pos = this.getTileLocation(width, height, cols, rows, tile.index);
      ctx.fillStyle = '#fff';
      ctx.fillRect(pos.x, pos.y, this.tile_size, this.tile_size);

      // Render tile border
      ctx.strokeStyle = '#000';
      ctx.lineWidth = 2;
      ctx.strokeRect(pos.x, pos.y, this.tile_size, this.tile_size);

      // Render tile number
      ctx.textAlign = "right";
      ctx.fillStyle = '#000';
      ctx.font = '10px sans-serif';
      const tile_index = sal.toDisplayIndex(tile.index);
      ctx.fillText(''+tile_index, pos.x + this.tile_size - ti_margin, pos.y+ti_y_offset);


      // Render tile name
      ctx.textAlign = "left";
      ctx.fillStyle = '#000';
      ctx.font = '10px sans-serif';
      this.fillWrappedText(ctx, tile.name, pos.x + name_margin, pos.y + ti_y_offset*1.8, this.tile_size - 2*name_margin, 4, 1);
    }

    // Render ladders and snakes
    for (const tile of sal.board.tiles) {
      if (tile.isSnake() || tile.isLadder()) {
        const to = sal.board.getTile(tile.goto);
        if (!to) continue;
        let t = this.getTileLocation(width, height, cols, rows, to.index);
        let f = this.getTileLocation(width, height, cols, rows, tile.index);

        // Instead of using corner or center arrow locations find a center offset
        // position so the text isn't obscured
        let l = this.getTileCardinalLocations(t, f);
        t = l.p1;
        f = l.p2;

        const arrow_color = tile.isSnake() ? '#e30000' : '#00c200';

        ctx.fillStyle = arrow_color;
        ctx.strokeStyle = arrow_color;
        this.renderArrow(ctx, f.x, f.y, t.x, t.y);
      }
    }

    // Render team locations

    // Group the teams together, so we know if there are any on the same tile
    const positions = new Map();
    for (const team of sal.teams) {
      if (!positions.has(team.current_tile_index))
        positions.set(team.current_tile_index, []);

      positions.get(team.current_tile_index).push(team);
    }

    // Actually render the team identifiers
    // TODO: break up a line if there are too many teams
    for (const [index, teams] of positions) {
      const tile = sal.board.getTile(index);
      if (!tile) continue;
      const t_pos = this.getTileLocation(width, height, cols, rows, tile.index);
      let pos = {
        x: t_pos.x + team_indicator_margin + team_indicator_r,
        y: t_pos.y + this.tile_size - (team_indicator_margin + team_indicator_r),
      };
      for (const team of teams) {
        ctx.fillStyle = team.color;
        this.fillCircle(ctx, pos.x, pos.y, team_indicator_r);

        pos.x += (2 * team_indicator_r) + team_indicator_space;
      }
    }

    return canvas;
  }

  giveTeamsColors(teams) {
    //TODO: actually create a color property on the teams that gets serialized and
    // has commands for setting it
    let colors = [...this.colors];
    for (const team of teams) {
      if (Object.prototype.hasOwnProperty.call(team, "color")) continue;

      if (colors.length > 0) {
        team.color = colors.shift();
        continue;
      }

      team.color = '#' + Math.floor(Math.random()*16777215).toString(16);
    }
  }

  fillCircle(ctx, x, y, r) {
    ctx.beginPath();
    ctx.arc(x, y, r, 0, 2*Math.PI, false);
    ctx.fill();
  }

  fillWrappedText(ctx, text, x, y, maxWidth, maxLines, line_height = 1.2) {
    // https://loga.nz/blog/measuring-line-height/

    // Wrap the text onto different lines
    const lines = this.getLines(ctx, text, maxWidth, maxLines);

    // Calculate pixel_line_height
    const measures = ctx.measureText('a');
    const pixel_font_size = measures.emHeightAscent + measures.emHeightDescent;
    const pixel_line_height = pixel_font_size * line_height;

    //
    let line_y = y;
    for (let i = 0; i < lines.length; i++) {
      ctx.fillText(lines[i], x, line_y);
      line_y += pixel_line_height;
    }
  }

  /**
   * Fill style controls head. Stroke style controls line
   * @param ctx
   * @param {int} from_x
   * @param {int} from_y
   * @param {int} to_x
   * @param {int} to_y
   * @param {int} size Changes the size of the arrow head
   */
  renderArrow (ctx, from_x, from_y, to_x, to_y, size = 5) {
    const angle = Math.atan2((to_y - from_y) , (to_x - from_x));
    const hyp = Math.sqrt((to_x - from_x) * (to_x - from_x) + (to_y - from_y) * (to_y - from_y));

    ctx.save();
    ctx.translate(from_x, from_y);
    ctx.rotate(angle);

    // line
    ctx.beginPath();
    ctx.moveTo(0, 0);
    ctx.lineTo(hyp - size, 0);
    ctx.stroke();

    // triangle
    ctx.beginPath();
    ctx.lineTo(hyp - size, size);
    ctx.lineTo(hyp, 0);
    ctx.lineTo(hyp - size, -size);
    ctx.fill();

    ctx.restore();
  }

  /**
   * A modified get lines that trims after a specific number of lines and adds
   * ellipsis when necessary.
   * @see https://stackoverflow.com/questions/2936112/text-wrap-in-a-canvas-element
   * @param ctx
   * @param text
   * @param maxWidth
   * @param maxLines
   * @return {*[]}
   */
  getLines(ctx, text, maxWidth, maxLines) {
    const words = text.split(" ");
    const lines = [];
    let currentLine = words[0];

    for (let i = 1; i < words.length; i++) {
      const word = words[i];
      const width = ctx.measureText(currentLine + " " + word).width;
      if (width < maxWidth) {
        currentLine += " " + word;
      } else {
        lines.push(currentLine);
        currentLine = word;
        if (lines.length === maxLines) break;
      }
    }

    // We have hit max lines, but there's still more to add
    if (lines.length === maxLines && currentLine !== '') {
      let line = lines.pop()+'...';
      currentLine = this.ellipsisTruncate(ctx, line, maxWidth);
    }

    lines.push(currentLine);
    return lines;
  }

  /**
   * If text width is greater than maxWidth then the text is truncated
   * and three dots are added to the end. (total width will be less than maxWidth)
   *
   * If always_add_ellipsis is true then an ellipsis will be added even if
   * the text width is less than maxWidth
   * @param ctx
   * @param text
   * @param maxWidth
   * @return {*|string}
   */
  ellipsisTruncate(ctx, text, maxWidth) {
    let width = ctx.measureText(text).width;
    if (width < maxWidth) return text;

    text = text.substring(0, text.length - 3) + '...';
    while (text.length > 3) {
      width = ctx.measureText(text).width;
      if (width < maxWidth) return text;
      text = text.substring(0, text.length-4) + '...';
    }

    //So we reduced the string to '...' and it still doesn't fit...
    // Throw an error?
    throw new Error(`maxWidth (${maxWidth}) too small to fit "..."`);
  }

  getTileLocation(width, height, cols, rows, index) {
    let col = index % cols;
    const row = Math.floor(index / cols);

    // Odd number rows go rtl so the board snakes back and forth
    if (row % 2 !== 0) {
      col = cols - (col+1);
    }

    const x = col * this.tile_size;
    const y = (height - (row * this.tile_size)) - this.tile_size;
    return {x: x, y: y};
  }

  getTileCardinalLocations(p1, p2, r) {
    if (!r) r = this.tile_size / 4;

    // Convert corner pos to center pos
    // TODO: cache half_tile_size somewhere?
    p1.x += this.tile_size/2;
    p1.y += this.tile_size/2;
    p2.x += this.tile_size/2;
    p2.y += this.tile_size/2;

    const p1cl = [
      {x: p1.x + r, y: p1.y},
      {x: p1.x - r, y: p1.y},
      {x: p1.x, y: p1.y + r},
      {x: p1.x, y: p1.y - r},
    ];

    const p2cl = [
      {x: p2.x + r, y: p2.y},
      {x: p2.x - r, y: p2.y},
      {x: p2.x, y: p2.y + r},
      {x: p2.x, y: p2.y - r},
    ];

    let min_a = null;
    let min_b = null;
    let min_dist = null;
    for (const a of p1cl) {
      for (const b of p2cl) {
        const dist = Math.hypot(b.x - a.x, b.y - b.y);
        if (min_dist === null || dist < min_dist) {
          min_a = a;
          min_b = b;
          min_dist = dist;
        }
      }
    }

    return {
      p1: min_a,
      p2: min_b,
    }
  }


}

export {SnakesAndLaddersRenderer}
