import {SapphireClient, container, LogLevel, RegisterBehavior, ApplicationCommandRegistries} from '@sapphire/framework';
import {GatewayIntentBits} from 'discord.js';
import fs from 'fs';
import {EventRepository} from "./Repository/EventRepository.js";
import {Cache} from "./Cache.js";
import 'dotenv/config';
import Database from "better-sqlite3";

ApplicationCommandRegistries.setDefaultBehaviorWhenNotIdentical(RegisterBehavior.BulkOverwrite);

if (process.env.GUILD_IDS) {
  const guildIds = process.env.GUILD_IDS.split(',');
  ApplicationCommandRegistries.setDefaultGuildIds(guildIds);
}

const client = new SapphireClient({
  intents: [GatewayIntentBits.MessageContent, GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages],
  logger: { level: LogLevel.Debug },
});

if (!fs.existsSync('./var')) {
  fs.mkdirSync('./var');
}

if (!fs.existsSync('./var/cache/salboards')) {
  fs.mkdirSync('./var/cache/salboards', {recursive: true});
}
container.image_cache_path = fs.realpathSync('./var/cache/salboards');

// Wire up the container
const db = new Database('./var/data.db');
db.pragma('journal_mode = WAL');
container.db = db;
container.cache = new Cache();
container.EventRepository = new EventRepository(db, container.cache);


client.login(process.env.LOGIN_TOKEN);
