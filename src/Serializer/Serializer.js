class Serializer {
  /**
   * Converts an object instance into a json object or string
   * @param object
   * @param {boolean} toString
   */
  serialize(object, toString = true) {}

  /**
   * Converts a data object or json string into the associated object instance
   */
  parse(data) {}
}

export {Serializer}
