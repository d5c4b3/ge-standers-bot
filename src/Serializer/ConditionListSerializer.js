import {Serializer} from "./Serializer.js";
import {CountFromListCondition} from "../DropCondition/CountFromListCondition.js";
import {OneFromListCondition} from "../DropCondition/OneFromListCondition.js";
import {SetCondition} from "../DropCondition/SetCondition.js";
import {SlottedSetCondition} from "../DropCondition/SlottedSetCondition.js";
import {ParseError} from "./ParseError.js";
import {AlwaysFalseCondition} from "../DropCondition/AlwaysFalseCondition.js";

class ConditionListSerializer extends Serializer {

  constructor() {
    super();
  }

  serialize(object, toString = true) {
    if (toString === false) throw new Error('toString cannot be false');

    switch (object.constructor.name) {
      case OneFromListCondition.name:
        return object.list.join('\n');
      case CountFromListCondition.name:
        return object.list.map(item => `${item.count} ${item.drop}`).join('\n');
      case SetCondition.name:
        return object.sets.map(set => set.join('\n')).join('\n\n');
      case SlottedSetCondition.name:
        return object.slots.map(slot => slot.name + ':\n' + slot.drops.join('\n')).join('\n\n');

    }
  }

  /**
   * @param {string} data
   * @param {string} condition
   */
  parse(data, condition) {
    if (condition === AlwaysFalseCondition.name) {
      return new AlwaysFalseCondition();
    }

    // see below for a set of example data
    let lines = data.split('\n');

    let groups = [];
    let curr_group = [];
    let set = null;

    for (let line of lines) {
      line = line.trim();

      // If we got an empty line then finish the current group
      if (line === '') {
        if (curr_group.length > 0) {
          groups.push(curr_group);
          curr_group = [];
        }

        continue;
      }

      // This is a Set identifier e.g. "Helm:"
      if (line.endsWith(':')) {
        set = line.slice(0, -1);
        continue;
      }

      // This is an item so create a struct to store data
      let item = {
        count: null,
        name: line,
        original: line,
        set: set,
      };

      // Check for count at beginning
      const parts = line.split(' ');

      // No count because item is one word e.g. "Bucket"
      if (parts.length === 1) {
        curr_group.push(item);
        continue;
      }

      const num = parseInt(parts[0]);

      // If num is a number add the count and update name to not include the number
      // Otherwise we just push the defaults
      if (!isNaN(num)) {
        item.count = num;
        parts.splice(0, 1);
        item.name = parts.join(' ');
      }

      curr_group.push(item);
    }

    if (curr_group.length > 0) {
      groups.push(curr_group);
    }


    //TODO: should we validate expectations? Such as all items in a Set MUST contain set identifiers
    // Or do we just trust the condition and let the create* methods deal with it?

    // TODO: we should probably validate condition to make sure it's one we support
    switch (condition) {
      case CountFromListCondition.name:
        return this.createCountFromList(groups);
      case OneFromListCondition.name:
        return this.createOneFromList(groups);
      case SetCondition.name:
        return this.createSet(groups);
      case SlottedSetCondition.name:
        return this.createSlottedSet(groups);
      default:
        throw new Error(`Unsupported Condition ${condition}`);
    }
  }

  createCountFromList(groups) {
    let items = [];
    for (const group of groups) {
      for (const item of group) {
        items.push({
          drop: item.name,
          count: item.count !== null ? item.count : 1,
        });
      }
    }
    return new CountFromListCondition(items);
  }

  createOneFromList(groups) {
    let items = [];
    for (const group of groups) {
      for (const item of group) {
        items.push(item.name);
      }
    }
    return new OneFromListCondition(items);
  }

  createSet(groups) {
    let sets = [];
    for (const group of groups) {
      sets.push(group.map(item => item.name));
    }
    return new SetCondition(sets);
  }

  createSlottedSet(groups) {
    let slots = [];
    for (const group of groups) {
      if (!group[0].set) throw new ParseError('Items in a slotted set must have a defined set');
      slots.push({
        // It shouldn't be possible for us to have an empty group, so I'm not going
        // to bother checking for it.
        name: group[0].set,
        drops: group.map(item => item.name),
      });
    }

    return new SlottedSetCondition(slots);
  }

  /**
   * CountFromList: - NO BLANK LINES ALLOWED (except trailing blank lines)
   * 2 Armadyl godsword
   * Abyssal whip
   * 6 Adamant full helm
   *
   * OneFromList: - Exact same as CountFromList, but if every item has count of one
   * Armadyl godsword
   * 1 Abyssal whip
   * 1 adamant full helm
   *
   * Set: - Empty lines are used to separate sets TODO: How to tell single set from OneFromList?
   * Ahrim's Hood
   * Ahrim's Robetop
   * Ahrim's robeskirt
   *
   * Dharok's helm
   * Dharok's platebody
   * Dharok's platelegs
   *
   * SlottedSet: - Same as Set, but first line *must* end with `:` and is the slot name
   * Helm:
   * Ahrim's Hood
   * Dharok's helm
   *
   * Body:
   * Dharok's platebody
   * Ahrim's robetob
   *
   * Legs:
   * Dharok's platelegs
   * Ahrim's robeskirt
   *
   * And/Or Not Implemented in this format
   */
}

export {ConditionListSerializer}
