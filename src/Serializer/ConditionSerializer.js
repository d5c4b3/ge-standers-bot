import {Serializer} from "./Serializer.js";
import {AlwaysFalseCondition} from "../DropCondition/AlwaysFalseCondition.js";
import {AndCondition} from "../DropCondition/AndCondition.js";
import {CountFromListCondition} from "../DropCondition/CountFromListCondition.js";
import {DropCondition} from "../DropCondition/DropCondition.js";
import {OneFromListCondition} from "../DropCondition/OneFromListCondition.js";
import {OrCondition} from "../DropCondition/OrCondition.js";
import {SetCondition} from "../DropCondition/SetCondition.js";
import {SlottedSetCondition} from "../DropCondition/SlottedSetCondition.js";

class ConditionSerializer extends Serializer {

  constructor() {
    super();

    this.conditions = new Map();

    //TODO: also register additional class properties that need to be serialized and
    // the method to do so.
    this.register(AlwaysFalseCondition);
    this.register(AndCondition);
    this.register(CountFromListCondition);
    this.register(DropCondition);
    this.register(OneFromListCondition);
    this.register(OrCondition);
    this.register(SetCondition);
    this.register(SlottedSetCondition);
  }

  register(condition_class) {
    this.conditions.set(condition_class.name, condition_class);
  }

  /**
   * @param {DropCondition} object
   * @param {boolean} toString
   */
  serialize(object, toString = true) {
    const data = {};
    data.con = object.constructor.name;
    data.params = [];

    switch (data.con) {
      case AndCondition.name:
      case OrCondition.name:
        data.params[0] = object.conditions.map(c => this.serialize(c, false));
        break;
      case CountFromListCondition.name:
        data.params[0] = object.list;
        break;
      case OneFromListCondition.name:
        data.params[0] = object.list;
        break;
      case SetCondition.name:
        data.params[0] = object.sets;
        break;
      case SlottedSetCondition.name:
        data.params[0] = object.slots;
    }

    return toString ? JSON.stringify(data) : data;
  }

  parse(data) {
    if (typeof data === "string")
      data = JSON.parse(data);

    // Don't directly call hasOwnProperty on json.
    // https://eslint.org/docs/latest/rules/no-prototype-builtins
    if (!Object.prototype.hasOwnProperty.call(data, "params")) {
      data.params = [];
    }

    // Parse sub conditions for And/Or
    switch (data.con) {
      case AndCondition.name:
      case OrCondition:
        data.params = data.params[0].map(c => this.parse(c));
        break;
    }

    // Make sure we have a constructor registered for the condition
    if (!this.conditions.has(data.con)) {
      throw Error(`Unable to parse condition: ${data.con}`);
    }

    return new (this.conditions.get(data.con))(...data.params);
  }
}

export {ConditionSerializer}
