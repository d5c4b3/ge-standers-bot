import {Serializer} from "../Serializer.js";
import {Team} from "../../SnakesAndLadders/Team.js";

class TeamSerializer extends Serializer {
  constructor() {
    super();
  }

  serialize(object, toString = true) {
    const data = {name: object.name, players: object.players, cti: object.current_tile_index, pti: object.previous_tile_index, rt: null};
    if (object.roll_time) data.rt = object.roll_time.toJSON();
    return toString ? JSON.stringify(data) : data;
  }

  parse(data) {
    if (typeof data === "string")
      data = JSON.parse(data);

    if (data.rt) data.rt = new Date(data.rt);

    return new Team(data.name, data.players, data.cti, data.pti, data.rt);
  }
}

export {TeamSerializer}
