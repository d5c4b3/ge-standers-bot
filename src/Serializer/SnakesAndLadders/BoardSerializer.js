import {Serializer} from "../Serializer.js";
import {Board} from "../../SnakesAndLadders/Board.js";
import {TileSerializer} from "./TileSerializer.js";

class BoardSerializer extends Serializer {
  constructor() {
    super();

    this.tileSerializer = new TileSerializer();
  }

  serialize(board, toString = true) {
    const data =  {
      tiles: board.tiles.map(t => this.tileSerializer.serialize(t, false)),
    };
    return toString ? JSON.stringify(data) : data;
  }

  parse(data) {
    if (typeof data === "string")
      data = JSON.parse(data);

    const tiles = data.tiles.map(t => this.tileSerializer.parse(t));

    // Add the indexes to the tiles data
    // See TileSerializer.parse
    for (let i = 0; i < tiles.length; i++) {
      tiles[i].index = i;
    }

    return new Board(tiles);
  }
}

export {BoardSerializer}
