import {Serializer} from "../Serializer.js";
import {SnakesAndLadders} from "../../SnakesAndLadders/SnakesAndLadders.js";
import {TeamSerializer} from "./TeamSerializer.js";
import {BoardSerializer} from "./BoardSerializer.js";

class SalSerializer extends Serializer {
  constructor() {
    super();

    this.teamSerializer = new TeamSerializer();
    this.boardSerializer = new BoardSerializer();
  }

  serialize(sal, toString= true) {
    const data = {
      name: sal.name,
      teams: sal.teams.map((team) => this.teamSerializer.serialize(team, false)),
      board: this.boardSerializer.serialize(sal.board, false),
    };
    return toString ? JSON.stringify(data) : data;
  }

  parse(data) {
    if (typeof data === "string")
      data = JSON.parse(data);

    const teams = data.teams.map((t) => this.teamSerializer.parse(t));
    const board = this.boardSerializer.parse(data.board);
    return new SnakesAndLadders(data.name, teams, board);
  }
}

export {SalSerializer}
