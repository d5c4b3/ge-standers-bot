import {Serializer} from "../Serializer.js";
import {ConditionSerializer} from "../ConditionSerializer.js";
import {Tile} from "../../SnakesAndLadders/Tile.js";

class TileSerializer extends Serializer {
  constructor() {
    super();

    this.conditionSerializer = new ConditionSerializer();
  }

  serialize(object, toString = true) {
    const data = {
      goto: object.goto,
      name: object.name,
      description: object.description,
      drops: object.drops,
      condition: null,
    };
    if (object.condition)
      data.condition = this.conditionSerializer.serialize(object.condition, false);

    // Only store the index if we are going directly to string
    // Otherwise parent can handle it
    if (toString) {
      data.index = object.index;
    }

    return toString ? JSON.stringify(data) : data;
  }

  parse(data) {
    if (typeof data === "string")
      data = JSON.parse(data);

    const condition = data.condition ? this.conditionSerializer.parse(data.condition) : null;
    return new Tile(data.index, data.name, data.description, condition, data.goto);
  }
}

export {TileSerializer}
