class Cache {

  // TODO: configurable max_items
  // TODO: probability based garbage collection on set/get
  constructor(default_ttl = 600) {
    this.items = {};
    this.aliases = {};
    this.seconds_to_expire = default_ttl;
  }

  remove(key) {
    if (!(key in this.items)) return;
    for (const alias of this.items[key].aliases) {
      delete this.aliases[alias];
    }
    delete this.items[key];
  }

  /**
   * @param {string} key
   * @return {*|null}
   */
  get(key) {
    if (!key) return null;
    if (!(key in this.items) && !(key in this.aliases)) return null;

    if (!(key in this.items)) key = this.aliases[key];

    if (Date.now() >= this.items[key].expires) {
      this.remove(key);
      return null;
    }

    return this.items[key].data;
  }

  /**
   * @param {string} key
   * @param data
   * @param {string[]|null} aliases
   * @param {number|null} seconds_to_expire
   */
  set(key, data, aliases = null, seconds_to_expire = null) {
    if (!seconds_to_expire) seconds_to_expire = this.seconds_to_expire;
    if (aliases === null) aliases = [];
    this.remove(key);
    this.items[key] = {data: data, expires: Date.now() + seconds_to_expire * 1000, aliases: aliases};
    for (const alias of aliases) {
      this.aliases[alias] = key;
    }
  }
}

export {Cache};
