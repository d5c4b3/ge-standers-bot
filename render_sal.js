import {SnakesAndLaddersRenderer} from "./src/Renderer/SnakesAndLaddersRenderer.js";
import {container} from "@sapphire/framework";
import fs from "fs";
import Database from "better-sqlite3";
import {Cache} from "./src/Cache.js";
import {EventRepository} from "./src/Repository/EventRepository.js";
import {Tile} from "./src/SnakesAndLadders/Tile.js";
import {Team} from "./src/SnakesAndLadders/Team.js";
import {AlwaysFalseCondition} from "./src/DropCondition/AlwaysFalseCondition.js";
import {SnakesAndLadders} from "./src/SnakesAndLadders/SnakesAndLadders.js";
import {Board} from "./src/SnakesAndLadders/Board.js";


const db = new Database('./var/data.db');
db.pragma('journal_mode = WAL');
const cache = new Cache();
const repo = new EventRepository(db, cache);

const guild_id = '490161971220709406';
const event = await repo.findOneByGuild(guild_id);

const teams = [];
for (let i = 0; i < 5; i++) {
  teams.push(new Team('Team '+(i+1), []));
}
const board = new Board([]);
const sal = new SnakesAndLadders('Lorm ipsum dolor sit amet', teams, board);

for (let i = 0; i < 84; i++) {
  const tile = new Tile(i, 'Tile ' + i, 'Tile description '+ i, new AlwaysFalseCondition());
  sal.insertTile(i, tile);
}
sal.insertTile(7, new Tile(7, 'Rev weapon, amulet or Ancient statuettes, or do whatever really idc it\'s fine', 'really long tile name', new AlwaysFalseCondition()));

sal.insertTile(2, new Tile(2, 'Goto 8', 'ladder', null, 7));
sal.insertTile(10, new Tile(10, 'Goto 5', 'ladder', null, 4));

for (const team of sal.teams) {
  team.moveTile(7);
}

const renderer = new SnakesAndLaddersRenderer();
const canvas = renderer.render(sal);
const buffer = canvas.toBuffer('image/png')
fs.writeFileSync('./sal.png', buffer);
